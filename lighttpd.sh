#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Konfiguration und starten des Lighthttpd "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
echo "Hostname ${HOSTNAME}"
cat ${LIGHTHTTP_TLS_KEY}  ${LIGHTHTTP_TLS_CERT} > /etc/lighttpd/${HOSTNAME}.pem
sed 's,##HOSTNAME##,'${HOSTNAME}',g' /config/lighttpd.conf > /etc/lighttpd/lighttpd.conf

# Make sure files/folders needed by the processes are accessible when they run under the lighttpd user
chown -R lighttpd:wheel /var/www/html
chown -R lighttpd:wheel /run/lighttpd
chown lighttpd:wheel /run/lighttpd.pid
chown -R lighttpd:wheel /var/run
#

