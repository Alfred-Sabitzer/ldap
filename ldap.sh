#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Konfiguration der ldap-Datenbank "
echo "#"
echo "############################################################################################"

cp -v -f /config/ldap.conf /etc/openldap/
cp -v -f /config/slapd.conf /etc/openldap/
cp -v -f /config/slapd.ldif /etc/openldap/

cp -v -f /config/config.inc.php /usr/share/webapps/phpldapadmin/config/config.php
chmod 644 /usr/share/webapps/phpldapadmin/config/config.php

cp -v -f /config/ldif/config/schema/*.ldif /etc/openldap/schema/
cp -v -f /config/Schemas/*.schema /etc/openldap/schema/

#cp -v -f /config/templates/creation/*.* /usr/share/webapps/phpldapadmin/templates/creation/
#cp -v -f /config/templates/modification/*.* /usr/share/webapps/phpldapadmin/templates/modification/
#cp -v -f /config/templates/creation/Sudo.xml /usr/share/webapps/phpldapadmin/templates/creation/
#cp -v -f /config/templates/modification/Sudo.xml /usr/share/webapps/phpldapadmin/templates/modification/
chown -R lighttpd:wheel /usr/share/webapps/phpldapadmin/*
chmod -R 777 /usr/share/webapps/phpldapadmin/templates/*

#rm -v -f /usr/share/webapps/phpldapadmin/templates/creation/SUSEPosixGroup.xml

chmod -R 644 /etc/openldap/*.conf
chmod -R 644 /etc/openldap/*.ldif
chmod -R 644 /etc/openldap/*.schema
chmod 755 /etc/openldap/schema
chmod -R 644 /etc/openldap/schema/*
chown -R ldap:ldap /etc/openldap/*

mkdir -p /run/openldap
touch /run/openldap/slapd.pid
chown ldap:ldap /run/openldap
chown ldap:ldap /run/openldap/slapd.pid

mkdir -p /run/openldap

# Something ugly - Eigene Keys
cp /certificate/tls.key /etc/ssl/private/ldap.slainte.at/server_key.pem
cp /certificate/tls.crt /etc/ssl/private/ldap.slainte.at/server_cert.pem

# Passwort neu setzen
pwd=$(slappasswd -s "${OPENLDAP_PASSWORD}")
sed -i 's,rootpw   ###ROOTPASSWORD###,rootpw  '${pwd}',g' /etc/openldap/slapd.conf
sed -i 's,olcRootPW: ###ROOTPASSWORD###,olcRootPW: '${pwd}',g' /etc/openldap/slapd.ldif

# Prüfen ob Erstinstallation
DIR="/var/lib/openldap/openldap-data"
FILE="ldap_installation_done.html"
if [ -d "$DIR" ]
then
	if [ ! -f $DIR/$FILE ]; then
	    echo "$DIR/$FILE existiert nicht. Neuinstallation"
	    chown -R ldap:ldap $DIR/*
	    chown -R ldap:ldap $DIR/*.*
      # Hier kommt der Teil, wo die DB erzeugt wird
      slapadd -f /etc/openldap/slapd.conf -l /etc/openldap/slapd.ldif
      slapadd -f /etc/openldap/slapd.conf -l /config/test.ldif
      # Test der Konfig
      slapd -f /etc/openldap/slapd.conf -h "ldaps:// ldap://" -r $DIR -T test
	    chown -R ldap:ldap $DIR/*
	    chown -R ldap:ldap $DIR/*.*
	    touch $DIR/$FILE
	    cat <<EOF >> $DIR/$FILE
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
     "http://www.w3.org/TR/html4/transitional.dtd">
<html>
<head>
  <meta HTTP-EQUIV=CONTENT-TYPE CONTENT="text/html; charset=utf-8">
  <title>ldap Installation Report</title>
</head>-v $ldap_dir:/var/lib/openldap/openldap-data
<body>
<center>
EOF
	    ls -lisa $DIR >> $DIR/$FILE
	    cat <<EOF >> $DIR/$FILE
</center>
</body>
</html>
EOF
	    echo "$DIR/$FILE erstellt"
	fi
else
	echo "Verzeichnis $DIR nicht gefunden."
	exit 1
fi

chmod -R 700 $DIR
chown -R ldap:ldap /run/openldap

echo "Suchen nach Files mit falschem Owner"
for f in $(find $DIR -type f \! -user ldap ) ; do
	chown -v ldap:ldap "${f}"
done
echo "Suchen nach Verzeichnissen mit falschem Owner"
for f in $(find $DIR -type d \! -user ldap ) ; do
	chown -v ldap:ldap "${f}"
done

echo "$DIR/$FILE existiert."
