#!/bin/bash
############################################################################################
#
# Richtiges Transformieren der Yaml-Files
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
image="ldap"
#
# Werte in den Setters richtig setzen
#
sed -i "s,image: .*,image: ${image},g" ./ldap-setters.yaml
sed -i "s,tag: .*,tag: latest,g" ./ldap-setters.yaml
#
# jetzt kommt kpt - läuft aber nicht unter arm64
#
kpt pkg tree
kpt fn eval --image gcr.io/kpt-fn/search-replace:v0.2.0 -- by-value-regex='\$\{image\}-(.*)' put-comment='kpt-set: ${image}-${1}'
kpt fn render
#kpt live init
#kpt live apply
#kpt live status