# k8s

## Description
Hiehr sind die Yamls für die K8S-Definitionen.
Mit Kpt wird das Package verändert (siehe Workflow im Kptfile).
Die Apply-Funktionen werden nicht verwendet, da die dahinterliegenden Imagages am Raspberry nicht funktionieren.
Es ist ausser dem schwierig das Repository im Cluster zu verwalten bzw. zu ändern.

`k get customresourcedefinitions.apiextensions.k8s.io resourcegroups.kpt.dev

NAME                     CREATED AT

resourcegroups.kpt.dev   2022-01-08T21:39:05Z

k get apiservices.apiregistration.k8s.io v1alpha1.kpt.dev

NAME               SERVICE   AVAILABLE   AGE

v1alpha1.kpt.dev   Local     True        46m

`

## Usage
Es werden nur die Funktionen

kpt pkg tree
kpt fn render

benutzt.

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] k8s`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree k8s`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init k8s
kpt live apply k8s --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
