#!/bin/bash
############################################################################################
#
# Bauen und deployen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
image="ldap"
ldap_dir="/tmp/ldap"
# Aktualisieren der Sourcen
git pull --ff-only
docker build --no-cache . -t ${image}:latest
#docker save ${image}:latest | gzip > ${image}_latest.tar.gz
docker kill $(docker ps | grep -i ${image}:latest | awk '{print $1 }') 
docker container rm -f $(docker container ls -a | grep -i ${image}_latest | awk '{print $1 }') 
docker run -d --name ${image}_latest -p 8080:80 -p 8443:443 -p 389:389 -p 636:636 -v $ldap_dir:/var/lib/openldap/openldap-data ${image}:latest
#docker run -d --name ${image}_latest -p 8080:80 -p 8443:443 -p 389:389 -p 636:636 ${image}:latest
sleep 10
docker ps
docker export $(docker ps | grep -i ${image}:latest | awk '{print $1 }') | gzip >  ${image}_latest_container.tar.gz
echo "docker exec -u 0 -it ${image}_latest sh -c \"clear; (bash || ash || sh) \""
#
