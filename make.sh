#!/bin/bash
############################################################################################
#
# Bauen und mit kpt deployen
#
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.registry:5000"
image="ldap"
datum=(`date '+%Y%m%d'`)
revision=(`date '+%H%M%S'`)
tag="${datum}-${revision}"
# Aktualisieren der Sourcen
git pull --ff-only
# Docker bauen und in das remote Repo pushen
docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:${tag}
docker build --force-rm . -t ${docker_registry}/${image}:latest
docker push ${docker_registry}/${image}:latest
curl ${docker_registry}/v2/${image}/tags/list
#
# jetzt kommt kpt - läuft aber nicht unter arm64
#
#cd ./kpt
#ls -lisa
#
# Werte in den Setters richtig setzen
#
#sed -i "s,image: .*,image: ${image},g" ./ldap-setters.yaml
#sed -i "s,tag: .*,tag: ${image},g" ./ldap-setters.yaml
#
#kpt pkg tree
#kpt fn eval --image gcr.io/kpt-fn/search-replace:v0.2.0 -- by-value-regex='\$\{image\}-(.*)' put-comment='kpt-set: ${image}-${1}'
#kpt fn render
#kpt live init
#kpt live apply
#kpt live status