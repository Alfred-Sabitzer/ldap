#! /bin/sh
# ###########################################################################################
#
# Erzeugen eines privaten Zertifikates
#
############################################################################################

umask 077

# ${site} is used for the subdir to hold the certs AND for
# the certificate's Common Name
common_name="slainte.at"
site="/etc/ssl/private/ldap.${common_name}"

ca_key="${site}/ca_key.pem"
ca="${site}/ca.pem"
server_key="${site}/server_key.pem"
server_req="${site}/server_req.pem"
server_cert="${site}/server_cert.pem"
server_fing="${site}/server_cert.fingerprint"
client_key="${site}/client_key.pem"
client_req="${site}/client_req.pem"
client_cert="${site}/client_cert.pem"
client_fing="${site}/client_cert.fingerprint"
keylen="4096"
DAYS="3650"

# certificate details
COUNTRY="AT"                    # 2 letter country-code
STATE="Steiermark"              # state or province name
LOCALITY="Unzmarkt-Frauenburg"  # Locality Name (e.g. city)
ORGNAME="Slainte"               # Organization Name (eg, company)
ORGUNIT="${common_name}"                      # Organizational Unit Name (eg. section)
EMAIL="microk8s.raspberry@slainte.at"    # certificate's email address
# optional extra details-key
CHALLENGE=""                    # challenge password
COMPANY="Slainte"               # company name

# Create clean environment
rm -rf ${site}
mkdir -p ${site} && cd ${site}
umask 277

# Create CA certificate
openssl genrsa ${keylen} > ${ca_key}
cat <<__EOF__ | openssl req -new -x509 -nodes -days ${DAYS} -key ${ca_key} -out ${ca}
$COUNTRY
$STATE
$LOCALITY
$ORGNAME
$ORGUNIT
${common_name}
$EMAIL
$CHALLENGE
$COMPANY
__EOF__

# Create server certificate, remove passphrase, and sign it
# ${server_cert} = public key, ${server_key} = private key
cat <<__EOF__ | openssl req -newkey rsa:${keylen} -nodes -keyout ${server_key} -out ${server_req}
$COUNTRY
$STATE
$LOCALITY
$ORGNAME
$ORGUNIT
${common_name}
$EMAIL
$CHALLENGE
$COMPANY
__EOF__

openssl rsa -in ${server_key} -out ${server_key}
openssl x509 -req -in ${server_req} -days ${DAYS} \
        -CA ${ca} -CAkey ${ca_key} -set_serial 01 -out ${server_cert}

# Create client certificate, remove passphrase, and sign it
# ${client_cert} = public key, ${client_key} = private key
cat <<__EOF__ | openssl req -newkey rsa:${keylen} -days ${DAYS} -nodes -keyout ${client_key} -out ${client_req}
$COUNTRY
$STATE
$LOCALITY
$ORGNAME
$ORGUNIT
${common_name}
$EMAIL
$CHALLENGE
$COMPANY
__EOF__

openssl rsa -in ${client_key} -out ${client_key}
openssl x509 -req -in ${client_req} -days ${DAYS} \
        -CA ${ca} -CAkey ${ca_key} -set_serial 01 -out ${client_cert}

# create fingerprint file
openssl x509 -fingerprint -in ${server_cert} -noout > ${server_fing}
openssl x509 -fingerprint -in ${client_cert} -noout > ${client_fing}

# Diffie-Helman parameter
openssl dhparam -out ${site}/dh.params 1024
#
chmod 777 ${site}
chmod 444 ${site}/*.*
#
# Statements um zu testen
#
# openssl x509 -in ${client_cert} -text -noout
# openssl s_client -connect localhost:443
# openssl s_client -connect localhost:389
# openssl s_client -connect localhost:636
# openssl s_client -connect localhost:636 -showcerts| head
# ldapsearch -x -ZZ  -d -1
# ldapsearch -x -b dc=slainte,dc=at -ZZ | head
# ldapsearch -v -x -D "cn=Manager,dc=slainte,dc=at" -w secret -b "dc=slainte,dc=at"  "(objectclass=*)"
# ldapsearch -v -x -D "cn=Manager,dc=slainte,dc=at" -w secret
# ldapsearch -H ldaps://localhost -d255
#
# openssl ciphers -v SECURE256:+SECURE128:-VERS-TLS-ALL:+VERS-TLS1.2:+VERS-DTLS1.2:-RSA:-DHE-DSS:-CAMELLIA-128-CBC:-CAMELLIA-256-CBC
#
# openssl ciphers -s -tls1_2
#
# openssl s_server -accept 1982 -cert /etc/ssl/private/ldap.slainte.at/server_cert.pem -key /etc/ssl/private/ldap.slainte.at/server_key.pem
# openssl s_client -connect localhost:1982
#
#
