#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Starten des LDAP-Containers "
echo "#"
echo "############################################################################################"

# Setzen der Passworte
export OPENLDAP_PASSWORD=$(cat /openldap/password)
export OPENLDAP_USER=$(cat /openldap/username)

# Erzeugen der Zertifikate
/cert.sh

# php.ini
cp -v /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
#sed -i 's,;extension=pgsql,extension=pgsql,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=pdo_pgsql,extension=pdo_pgsql,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=gd,extension=gd,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=intl,extension=intl,g' /usr/local/etc/php/php.ini
#sed -i 's,;extension=ldap,extension=ldap,g' /usr/local/etc/php/php.ini
sed -i 's,;gd.jpeg_ignore_warning = 1,gd.jpeg_ignore_warning = 1,g' /usr/local/etc/php/php.ini
sed -i 's,output_buffering =.*,output_buffering = off,g' /usr/local/etc/php/php.ini
sed -i 's,;upload_tmp_dir =.*,upload_tmp_dir = /var/www/html/upload_tmp_dir,g' /usr/local/etc/php/php.ini
sed -i 's,upload_max_filesize =.*,upload_max_filesize = 20M,g' /usr/local/etc/php/php.ini
sed -i 's,post_max_size =.*,post_max_size = 60M,g' /usr/local/etc/php/php.ini
#
mkdir -p /var/www/html/upload_tmp_dir

# Konfigurieren des lighttpd
/lighttpd.sh

# Konfigurieren der ldap-db
/ldap.sh

echo "############################################################################################"
echo "#"
echo "# Starten des Supervisors "
echo "#"
echo "############################################################################################"
# Starten des Supervisors
mkdir -p /etc/supervisor/conf.d
cp -v -f /config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Start Prozesse mit Ausgabe auf stdout und stderr
chmod o+rw /dev/stdout
chmod o+rw /dev/stderr
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
#
#/dummy.sh