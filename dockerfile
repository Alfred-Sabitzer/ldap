FROM docker.registry:5000/php8:latest as builder
#FROM php8:latest as builder

FROM alpine:latest

LABEL maintainer="microk8s.raspberry@slainte.at"
LABEL Description="LDAP Container"

# timezone support
ENV TZ=Europe/Vienna
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/${TZ} /etc/localtime
RUN echo ${TZ} > /etc/timezone

# Seit neuestem wird auf UTC zurückgeschalten, wenn das /usr/share/zoneinfo nicht da ist.
#RUN mkdir -p /tmp/${TZ}
#RUN mv /usr/share/zoneinfo/${TZ} /tmp/${TZ}
#RUN rm -rf /usr/share/zoneinfo
#RUN mkdir -p /usr/share/zoneinfo/Europe
#RUN mv /tmp/${TZ} /usr/share/zoneinfo/${TZ}

COPY --from=builder /etc /etc
COPY --from=builder /lib /lib
COPY --from=builder /sbin /sbin
COPY --from=builder /usr/bin /usr/bin
COPY --from=builder /usr/lib /usr/lib
COPY --from=builder /usr/libexec /usr/libexec
COPY --from=builder /usr/local /usr/local
COPY --from=builder /usr/sbin /usr/sbin
COPY --from=builder /var /var

ENV LIBRARY_PATH=/lib:/usr/lib

RUN sed --in-place '/lighttpd/d' /etc/shadow
RUN sed --in-place '/lighttpd/d' /etc/group
RUN sed --in-place '/lighttpd/d' /etc/passwd

# Create appuser
RUN adduser \
    --disabled-password \
    --gecos "User for lighttpd und fpm" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    "lighttpd"

ENV LIGHTHTTP_TLS_CERT=/certificate/tls.crt
ENV LIGHTHTTP_TLS_KEY=/certificate/tls.key
ENV LIGHTTPD_INC=/config/lighttpd.conf
# Default values for initial setup of OpenLDAP
ENV LDAP_DOMAIN=slainte.at
ENV LDAP_ORGANIZATION=slainte
ENV LDAP_ADMIN_PASSWORD=changoninstall

# Per default the config database is accessible only locally
# Set these values to allow remote access as well.
# Se LDAP_CONFIG_DN *must* be under "cn=config"
ENV LDAP_CONFIG_DN=cn=admin,cn=config
ENV LDAP_CONFIG_PASSWORD=changeoninstall

# The log level. See http://www.openldap.org/doc/admin24/slapdconf2.html for details.
ENV LDAP_LOG_LEVEL=${LDAP_LOG_LEVEL:-256}

# Install packages
RUN apk add \
	ca-certificates \
	coreutils \
	curl \
	fcgi \
	lighttpd \
	mc \
	openldap \
	openldap-back-mdb \
	openldap-clients \
	openssl \
	perl \
	psmisc \
	supervisor \
	unixodbc \
	wget \
    php7-cgi \
    php7-gettext

RUN php -m

# Create Certificates
COPY cert.sh /
RUN  chmod +x /cert.sh

# Create directories
RUN mkdir -p /run/lighttpd/ && \
	mkdir -p /var/lib/lighttpd

RUN mkdir -p /usr/share/webapps/
ADD phpLDAPadmin-1.2.6.3.tar.gz /usr/share/webapps/
RUN ln -s /usr/share/webapps/phpLDAPadmin-1.2.6.3 /usr/share/webapps/phpldapadmin

RUN chmod -R 777 /usr/share/webapps/
RUN chown -R lighttpd:wheel /usr/share/webapps/
RUN chown -R lighttpd:wheel /run/lighttpd/
RUN chown -R lighttpd:wheel /var/lib/lighttpd/
RUN chown -R lighttpd:wheel /var/log/lighttpd/

ADD config /config/
ADD certificate /certificate/

# Lighttpd ports
EXPOSE 80/tcp
EXPOSE 443/tcp

# LDAP port exposed
EXPOSE 389/tcp
EXPOSE 636/tcp

COPY openldap/ /openldap
COPY dummy.sh /
COPY lighttpd.sh /
COPY ldap.sh /
COPY start.sh /
RUN chmod +x /*.sh

# Starten mit den richtigen Vorarbeiten
CMD ["/start.sh"]

